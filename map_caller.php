<?php 

$query_string = http_build_query($_GET);

$curl = curl_init();
curl_setopt_array($curl, array(
	CURLOPT_RETURNTRANSFER => 1,
	CURLOPT_URL => 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?'.$query_string
	));

$result = curl_exec($curl);

if(!curl_exec($curl)){
	die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
}
echo $result;
?>